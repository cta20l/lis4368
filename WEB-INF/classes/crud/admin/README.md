> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications Development

## Christian T. Ash

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repos
    - Complete Bitbucket tutorials 
    (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - A website with its content stored in HTML files is referred to as a "static" website
    - whereas a data-driven web application is referred to as a "dynamic" website. 
    -Write a Database Servlet Deploying Servlet using @WebServlet

3. [A3 README.md](a3/README.md "My A3 README.md file")

 - Created and submitted in MySQL Workbench format (.mwb)
 - Record, track, and maintain relevant company data
    1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer. 
    2. A store has many pets, but each pet is sold by only one store. 
 
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a Bootstrap lient-side validation
    - Test Failed and Passed the Bootstrap client-side validation
    - Skillsets (7-9):
        7. Q7 - Count Characters
        8. Q8 - ASCII
        9. Q9 - Grade Calculator


5. [A4 README.md](a4/README.md "My A4 README.md file")

    - Skillsets (10-12):
        7. Q10 - Simple_Calculator
        8. Q11 - Compound Interest Calculator
        9. Q12 - Array Copy


6. [A5 README.md](a5/README.md "My A5 README.md file")

7. [P2 README.md](p2/README.md "My P2 README.md file")