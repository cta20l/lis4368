> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applocations Development

## Christian T Ash

### Assignment 5 Requirements:

 * In order to implement an application, use Model View Controllers
 *  Along with CRUD modeling utilizing Servlets
 *  Skillsets (13-15):
    *  Q13 - File Write Read Count Words
    *  Q14 - Vehicle Demo
    *  Q15 - Car Inherits Vehicle


#### README.md file should include the following items:


####  Assignment 5 Screenshots:

#### Data Entry with Server Side Validation
* Before
![PA5 Passed Output](img/Picture1.png)

* After
![A5 Passed Input](img/Picture2.png)

#### A5 Command Prompt Input and Output:
* Before
![A5 Passed Input](img/Picture3.png)
* After
![A5 Passed Output](img/Picture4.png)


#### Skillsets Screenshot:
* Skillset Q13 - File Write Read Count Words:

![ SkillSet 13 Screenshot](img/skill13.png)

* Skillset Q14 - Vehicle Demo

![ SkillSet 14 Screenshot](img/skill14.png)

* Skillset Q15 - Car Inherits Vehicle

![ SkillSet 15 Screenshot](img/skill15.png)


#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis4368/src/master/ "Bitbucket Repo Link")
