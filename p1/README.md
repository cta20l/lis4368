> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applocations Development

## Christian T Ash

### Project 1 Requirements:


#### README.md file should include the following items:


#### Project 1 Screenshots:
* P1  Input:
![P1 Input](img/Picture1.png)

* P1 Failed Output:
![P1 Failed](img/Picture2.png)

* P1 Passed Output:
![P1 Passed](img/Picture3.png)


#### Skillsets Screenshot:
* Skillset Q7 - Q7 Count Characters:

![ SkillSet 7 Screenshot](img/skill7.png)

* Skillset Q8 - ASCII:

![ SkillSet 8 Screenshot](img/skill8.png)

* Skillset Q9 - Grade Calculator:

![ SkillSet 9 Screenshot](img/skill9.png)


#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis4368/src/master/ "Bitbucket Repo Link")
