> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applocations Development

## Christian T Ash

### Assignment 4 Requirements:


#### README.md file should include the following items:


####  Assignment Screenshots:

*A4 Failed User Entry
![P4 Failed Input](img/Picture2.png)
![P4 Failed Output](img/Picture1.png)

* A4 Passed Output:
![P4 Passed Input](img/Picture3.png)
![P4 Passed Output](img/Picture4.png)


#### Skillsets Screenshot:
* Skillset Q10 - Simple Calculator:

![ SkillSet 10 Screenshot](img/skill10.png)

* Skillset Q11 - Compound Interest Calculator:

![ SkillSet 11 Screenshot](img/skill11.png)

* Skillset Q12 - Array Copy:

![ SkillSet 12 Screenshot](img/skill12.png)


#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis4368/src/master/ "Bitbucket Repo Link")
