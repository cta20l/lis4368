import java.util.Scanner;
import java.io.File;

class Methods
{
  
	
	public static void getRequirements()
   {
	   System.out.println("Developer: Christian Ash");
	   System.out.println("Program lists files and subdirectories of user-specified directory.");
	   
	    System.out.println();
   }
   
   public static void directoryInfo()
   {
	String myDir = "";
	Scanner sc = new Scanner(System.in);

	System.out.println("Please enter directory path: ");
	myDir = sc.nextLine();

	File directoryPath = new File(myDir);

	File filesList[] = directoryPath.listFiles();

	System.out.println("List and files and directorties in specified directory:");
	for(File file : filesList)
	{
		System.out.println("Name: "+ file.getName());
		System.out.println("Path: "+file.getAbsolutePath());
		System.out.println("Size (Bytes): "+ file.length());
		System.out.println("Size (KB): "+ file.length()/(1024));
		System.out.println("Size (MB): "+file.length()/(1024 * 1024));
		System.out.println();
	}
	System.out.println("--------------------------------------------------");

	sc.close();
   }
}