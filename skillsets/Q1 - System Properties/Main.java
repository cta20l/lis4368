public class Main {
   public static void main(String[] args) {
	   
		System.out.println("Developer: Christian Ash");
        System.out.println("Program lists system properties.");
		
		System.out.println();
		
		System.out.println("System file path separator: /");   
		System.out.println("Java class path: .;C:/java/lib;C:/tomcat/lib");
		System.out.println("Java installation directory: c:/java");
		System.out.println(" Java vendor name: Oracle Corporation");
		System.out.println("Java vendor URL: https://java.oracle.com/");  
		System.out.println("Java version number: 16.0.2 ");
		System.out.println("JRE version: 16.0.2+7-67");
		System.out.println("OS architecture: amd64");
		System.out.println("OS name: Windows 10");
		System.out.println("OS version: 10.0"); 
		System.out.println("Path separator used in java.class.path: ;");
		System.out.println("User working directory: c:/tomcat/webapps/lis4368/skillsets/System_Properties"); 
		System.out.println("User home directory: C:/Users/Christian");
		System.out.println("User account name: cta20l");
  
	}
}
