public class Main {   // Save as "Hello.java" under "d:\myProject"
   public static void main(String[] args) {
	   
		System.out.println("Developer: Christian Ash");
        System.out.println("Program loops through array of floats.");
		System.out.println("Use following values: 1.0, 2.1, 3.2., 4.3, 5.4.");
		System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
  

		System.out.println();
	
		float[] val = {1.0f,2.1f,3.2f,4.3f,5.4f};
		int arraySize = val.length;
		
		System.out.println("for loop: ");
			for(int i=0; i<arraySize; i++)
			{
				System.out.println(val[i]);
			}
			System.out.println();
	
		System.out.println("Enhanced for loop: ");
		for(Float values: val)
		{
			System.out.println(values);
		}
		System.out.println();
		
		System.out.println("while loop: ");
	
		int f=0;
		while(f<arraySize){
		
			System.out.println(val[f]);
			f++;
		}
		System.out.println("");
	
		int d=0;
		System.out.println("do...while loop: ");
		do{
			System.out.println(val[d]);
			d++;
		}
			while(d<arraySize);
			System.out.println();
	}
	
}