import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Chhristian Ash");
        System.out.println("Program uses methods to:");
        System.out.println("add, subtract, multiply, divide and power floating point numbers, rounded to two decimal places.");
        System.out.println("Note: Program checks for non-numeric values, and division by zero.");
        System.out.println();
    }
    public static void calculateNumbers()
    {
        double num1 = 0.0, num2 = 0.0;
        char operation = ' ';
        Scanner sc = new Scanner(System.in);

       // for(int i=0; i>0; i++){
        //System.out.println("//Run #"+i+":");
        System.out.print("Enter mathematical operation (a=addition, s=subtraction, m=multipulcation, d=division, e=exponentiation): ");
        operation = sc.next().toLowerCase().charAt(0);


        while(operation != 'a' && operation != 's' && operation != 'm' && operation != 'd' && operation != 'e')
        {
            System.out.print("\nIncorrect operation. Please enter correct operation: ");
            operation = sc.next().toLowerCase().charAt(0);
        }

        System.out.print("Please enter first number: ");
        while(!sc.hasNextDouble()){
            System.out.print("Not valid number!\n");
            sc.next();
            System.out.print("\nPlease Try again. Enter first number: ");
               }
            num1=sc.nextDouble();
        System.out.print("Please enter second number: ");
        while(!sc.hasNextDouble()){
            System.out.print("Not valid number!\n");
            sc.next();
            System.out.print("\nPlease Try again. Enter second number: ");
                }
            num2=sc.nextDouble();
        
            if(operation == 'a'){
                Addition(num1,num2);
            }
            else if(operation == 's'){
                Subtraction(num1,num2);
            }
            else if(operation == 'm'){
                Multiplication(num1,num2);
            }
            else if(operation == 'd'){
                if(num2==0){
                    System.out.println("Cannot divide by zero!");
                }
                else{
                Division(num1,num2);
            }
            }
            else if(operation == 'e'){
                Exponentiation(num1,num2);
                }
            System.out.println();
            sc.close();
        }
//}
public static void Addition(Double n1, double n2){
    System.out.print(n1 + " + " + n2 + " = ");
    System.out.printf("%.2f", (n1 + n2));
}

public static void Subtraction(Double n1, double n2){
    System.out.print(n1 + " - " + n2 + " = ");
    System.out.printf("%.2f", (n1 - n2));
}

public static void Multiplication(Double n1, double n2){
    System.out.print(n1 + " * " + n2 + " = ");
    System.out.printf("%.2f", (n1 * n2));
}

public static void Division(Double n1, double n2){
    System.out.print(n1 + " / " + n2 + " = ");
    System.out.printf("%.2f", (n1 / n2));
}

public static void Exponentiation(Double n1, double n2){
    System.out.print(n1 + " to the power of " + n2 + " = ");
    System.out.printf("%.2f", (Math.pow(n1,n2)));
}
}


