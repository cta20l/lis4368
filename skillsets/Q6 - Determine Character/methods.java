import java.util.Scanner;

class Methods
{
  
  
   public static void getRequirements()
   {
	
      System.out.println("Developer: Christian Ash");
	   System.out.println("Program determines whether user-entered value is vowel, consonant, special character, or integer."); 
	   System.out.println("Program displays character's ASCII value.");
	   
      System.out.println("\nReferences: \n"
      +"ASCII Background: https://en.wikipedia.org/wiki/ASCII\n"
      +"ASCII Character Table: https://www.ascii-code.com/\n" 
      +"Lookup Tables: https://www.lookuptables.com/");

	   System.out.println();
   }
   
   public static void determineChar()
   {

   char ch = '\u0000';
   char chTest = '\u0000';
   Scanner sc = new Scanner(System.in);

   for(int i=1;i<=6;i++)
   {
   System.out.print("\n//Run #"+i+": ");
   System.out.print("\nPlease enter character: ");

   ch = sc.next().charAt(0);
   chTest = Character.toLowerCase(ch);

   if((chTest == 'a' || chTest == 'e' || chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y' ))
      {
	      System.out.print(ch + " is vowel. ASCII value: " + (int) ch);
      }
   
   else if(ch >= '0' && ch <= '9')
      {
	      System.out.print(ch +" is integer. ASCII value: " + (int)ch);
      }
   
   else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
   {
	System.out.print(ch +" is special character. ASCII value: " + (int)ch);
   }
}
	sc.close();
   }
   
}