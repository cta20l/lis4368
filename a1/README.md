> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 _ Advanced Web Applocations Development

## Christian T Ash

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Sevlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running 
* git commands w/sho
* Bitbucket rep links a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add -  Add one or more files to staging (index)
4. git commit - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository:
6. git pull -Fetch and merge changes on the remote server to your working directory
7. git fetch origin - Instead, to drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Screenshot1.png)

*Screenshot of running java Hello http://localhost:9999*

![AMPPS Installation Screenshot 1](img/Screenshot7.png)
![AMPPS Installation Screenshot 1](img/Screenshot2.png)
![AMPPS Installation Screenshot 2](img/Screenshot3.png)
![AMPPS Installation Screenshot 3](img/Screenshot4.png)

#### Assignment 1 Screenshot:

![AMPPS Installation Screenshot 4](img/Screenshot6.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ctawork/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
