> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applocations Development

## Christian T Ash

### Assignment 2 Requirements:


#### README.md file should include the following items:
* Assessment: the following links: 
    1. [http://localhost:9999/hello](http://localhost:9999/hello)
    2. [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)
    3. [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello) (invokes HelloServlet) 
    4. [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html) 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Screenshot1.png)

*Screenshot of running java Hello http://localhost:9999*

![AMPPS Installation Screenshot 1](img/Screenshot2.png)

*Screenshot of running java say Hello http://localhost:9999*

![AMPPS Installation Screenshot 2](img/Screenshot3.png)

*Screenshot of running java Hello http://localhost:9999/hello/querybook.html

![AMPPS Installation Screenshot 3](img/Screenshot4.png)
![AMPPS Installation Screenshot 4](img/Screenshot5.png)

#### Assignment 2 Screenshot:

![AMPPS Installation Screenshot 5](img/Screenshot6.png)

#### SkillSet Screenshot:
* Skillset Q1 - System Properties: 

![ SkillSet 1 Screenshot](img/Screenshot8.png)

* Skillset Q2 - Looping Structures: 

![ SkillSet 2 Screenshot](img/Screenshot9.png)

* Skillset Q3 - Number Swap: 

![ SkillSet 3 Screenshot](img/Screenshot7.png)


#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
<<<<<<< HEAD
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis4368/src/master/ "Bitbucket Repo Link")
=======
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis3781/src/master/ "Bitbucket Repo Link")
>>>>>>> 3fd5ff9c2d14b639096c11175323c0bd3cc3a919
