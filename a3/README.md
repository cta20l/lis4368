> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applocations Development

## Christian T Ash

### Assignment 3 Requirements:


#### README.md file should include the following items:


#### Assignment Screenshots:
* A3 Model Input:
![A3 Model](img/a3_model.png)

* A3 Model Output:
![A3 Model PetStore](img/a3_petstore.png)

![A3 Model Pet](img/a3_pet.png)

![A3 Model Customer](img/a3_customer.png)

#### Assignment 3 on MYSQL:

* A3 MYSQL Pet and PetStore:
![A3 Model](img/mysql1.png)

* A3 MYSQL Customer:
![A3 Model](img/mysql2.png)

#### Assignment 3 on Locacl Server Screenshot:

![A3 Index](img/a3_index.png)

#### SkillSet Screenshot:
* Skillset Q4 - Directory Info:

![ SkillSet 4 Screenshot](img/Skill4.png)

* Skillset Q5 - Character Info:

![ SkillSet 5 Screenshot](img/Skill5.png)

* Skillset Q6 - Determine Character:

![ SkillSet 6 Screenshot](img/Skill6.png)

*A3 docs: a3.mwb and a3.sql*: 
 
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format") 
 
[A3 SQL File](docs/a3.sql "A3 SQL Script")


#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis4368/src/master/ "Bitbucket Repo Link")
