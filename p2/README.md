> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applocations Development

## Christian T Ash

### Project 2 Requirements:

- To implement CRUD functionality, create a web application using JSP/Servlets.
- Introducing the SCRUD option for searching data.
- Deliverables including associated screenshots.

#### README.md file should include the following items:


####  Project 2 Screenshots:

#### Data Entry with Valid User Form Entry
* Before
![P2 Data Entry](img/Picture_1.png)

* After
![P2 Data Entry](img/Picture_2.png)

#### Displaying Data

![A5 Passed Input](img/Picture_3.png)

#### Modifying Form

![P2 Passed Input](img/Picture_5.png)
![P2 Passed Input](img/Picture_6.png)

#### Deleting Data

![A5 Passed Input](img/Picture_4.png)

#### SQL Tables Database Changes 

* Select
![P2 Passed Input](img/Picture_7.png)
* Insert
![P2 Passed Input](img/Picture_8.png)

* Update
![P2 Passed Input](img/Picture_9.png)
* Delete
![P2 Passed Input](img/Picture_10.png)

#### Repo Links:

*Bitbucket Repo Link  - Station Locations:*
[Bitbucket Repo Link](https://bitbucket.org/ctawork/lis4368/src/master/ "Bitbucket Repo Link")
